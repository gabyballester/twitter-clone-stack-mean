// Modulos necesarios
import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";

// Componentes
import { MainComponent } from "./components/main/main.component";
import { AddComponent } from "./components/add/add.component";
import { ReceivedComponent } from "./components/received/received.component";
import { SentComponent } from "./components/sent/sent.component";

import {UserGuard} from '../services/user.guard';

// Creamos constante con las Rutas del tipo Routes con un array
const messagesRoutes: Routes = [
  {
    path: "mensajes", // ruta padre
    component: MainComponent, // componente padre
    children: [
      //rutas hijas
      { path: "", redirectTo: "recibidos", pathMatch: "full" },
      { path: "enviar", component: AddComponent, canActivate:[UserGuard]},
      { path: "recibidos", component: ReceivedComponent, canActivate:[UserGuard] },
      { path: "recibidos/:page", component: ReceivedComponent, canActivate:[UserGuard] },
      { path: "enviados", component: SentComponent, canActivate:[UserGuard] },
      { path: "enviados/:page", component: SentComponent, canActivate:[UserGuard] },
    ],
  },
];

@NgModule({
  imports: [
    //esto se aplicará a las rutas globales
    RouterModule.forChild(messagesRoutes),
  ],
  exports: [
    //exportamos para usarlo fuera del módulo
    RouterModule,
  ],
})
export class MessagesRoutingModule {}

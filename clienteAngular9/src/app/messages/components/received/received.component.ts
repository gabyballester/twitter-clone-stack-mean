import { Component, OnInit, DoCheck } from "@angular/core";
import { Router, ActivatedRoute, Params } from "@angular/router";
import { Message } from "../../../models/message";
import { MessageService } from "../../../services/message.service";
import { User } from "../../../models/user";
import { UserService } from "../../../services/user.service";
import { Follow } from "../../../models/follow";
import { FollowService } from "../../../services/follow.service";
import { GLOBAL } from "../../../services/global"; //Importamos el GLOBAL

@Component({
  selector: "received",
  templateUrl: "./received.component.html",
  styleUrls: ["./received.component.scss"],
  providers: [FollowService, MessageService, UserService],
})
export class ReceivedComponent implements OnInit {
  public title: string;
  public message: Message;
  public identity;
  public token;
  public url: string;
  public status: string;
  public follows;
  public messages: Message[]; //tipo array de mensajes
  public pages;
  public total;
  public page;
  public next_page;
  public prev_page;

  constructor(
    private _route: ActivatedRoute,
    private _router: Router,
    private _followService: FollowService,
    private _messageService: MessageService,
    private _userService: UserService
  ) {
    this.title = "Mensajes recibidos";
    // le pasamos todos los espacios de propiedades de message
    this.identity = this._userService.getIdentityFromLocalStorage();
    this.token = this._userService.getTokenFromLocalStorage();
    this.url = GLOBAL.url;
  }

  ngOnInit() {
    console.log("Received component cargado");
    this.actualPage();
  }

  actualPage() {
    //Recogemos el parámetro page
    this._route.params.subscribe(params => {
      let page = +params['page']; //recogemos el parámetro page como entero con el signo +
      this.page = page; //damos valor a esa propiedad

      if (!params['page']) {
        page = 1;
      }

      if (!page) {    //Si la página no existe
        page = 1; //la pagina es = 1
      } else { //si existe
        this.next_page = page + 1; //la siguiente página será page + 1
        this.prev_page = page - 1; //la página previa será page - 1

        if (this.prev_page <= 0) {    //si la pgina previa es <= 0 es porque es la primera,
          this.prev_page = 1; //entonces la pagina previa será 1
        }

      }
    //llamamos a getMessages que traerá el objeto messages
    this.getMessages(this.token, this.page); // le paso token y page
    });

  }

  getMessages(token, page) {
    this._messageService.getMyMessages(token, page).subscribe(
      response => {
        if (response.messages) {
          this.messages = response.messages;
          this.total = response.total;
          this.pages = response.pages;
        }
      },
      error => {
        console.log(error);
      }
    );
  }
}
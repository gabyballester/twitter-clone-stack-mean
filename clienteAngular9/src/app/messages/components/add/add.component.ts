import {Component, OnInit, DoCheck} from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Message } from '../../../models/message';
import { MessageService} from '../../../services/message.service';
import { User } from '../../../models/user';
import { UserService } from '../../../services/user.service';
import { Follow } from '../../../models/follow';
import { FollowService } from '../../../services/follow.service';
import { GLOBAL } from '../../../services/global'; //Importamos el GLOBAL

@Component({
    selector: 'add',
    templateUrl: './add.component.html',
    styleUrls: ['./add.component.scss'],
    providers:[FollowService, MessageService, UserService]
})

export class AddComponent implements OnInit{
    public title:string;
    public message: Message;
    public identity;
    public token;
    public url: string;
    public status: string;
    public follows; // para poder enviar mensajes sólo a quien seguimos

    constructor(
        private _route: ActivatedRoute,
        private _router: Router,
        private _followService: FollowService,
        private _messageService: MessageService,
        private _userService: UserService
    ){
        this.title = 'Enviar mensajes'
        // le pasamos todos los espacios de propiedades de message
        this.identity = this._userService.getIdentityFromLocalStorage();
        this.token = this._userService.getTokenFromLocalStorage();
        this.url = GLOBAL.url;
        this.message = new Message('','','','',this.identity._id,'');
    }

    ngOnInit() {
        console.log("Add component cargado");
        this.getMyFollows();
      }

        // Creamos el método onSubmit para el envío del formulario
  onSubmit() {
    console.log(this.message);
    this._messageService.addMessage(this.token, this.message).subscribe(
      (response) => {
        // si todo va bien
        if (response.message) {
          // si recibo el objeto
          this.status = "success"; // estado exitoso
        }
      },
      (error) => {
        this.status = "error";
        console.log(<any>error);
      }
    );
  }



    

      // Comprobamos que el message llega con el texto que necesitamos
  getMyFollows(){
    this._followService.getMyFollows(this.token).subscribe(
        response=>{
          this.follows = response.follows;
        }, error => {
            console.log(<any>error);
        }
    );
}

}

//Definimos objetos
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { GLOBAL } from './global';
import { Publication } from '../models/publication';

@Injectable()
export class PublicationService {
    public url: string;

    constructor(private _http: HttpClient) {
        this.url = GLOBAL.url;
    }

    /**   Método para añadir una publicación
     *  recibimos token y el objeto publication
     * A la respuesta a este método recibimos un observable any
    */
    addPublication(token, publication): Observable<any> {

        let params = JSON.stringify(publication);    //estringifeamos el objeto
        let headers = new HttpHeaders() //seteamos cabeceras
            .set('Content-Type', 'application/json')  //tipo json
            .set('Authorization', token);   //autorization con token

        return this._http.post(this.url + 'publication', params, { headers: headers }); //retornamos lo que devuelve el api 
    }

    getPublications(token, page = 1): Observable<any> {

        let headers = new HttpHeaders() //seteamos cabeceras
            .set('Content-Type', 'application/json')  //tipo json
            .set('Authorization', token);   //autorization con token

        return this._http.get(this.url + 'publications/' + page, { headers: headers });
    }

    //conseguir las publicaciones de un usuario concreto y le pasámos la página
    getPublicationsUser(token, user_id, page = 1): Observable<any> {
        
        let headers =  new HttpHeaders() //seteamos cabeceras
            .set('Content-Type', 'application/json')  //tipo json
            .set('Authorization', token);   //autorization con token
                
        return this._http.get(this.url + 'publications-user/' + user_id + '/' + page, { headers: headers });
    }


    deletePublication(token, id): Observable<any> {
        
        let headers = new HttpHeaders()  //seteamos cabeceras
            .set('Content-Type', 'application/json')  //tipo json
            .set('Authorization', token);   //autorization con token

        return this._http.delete(this.url + 'publication/' + id, { headers: headers });
    }
}
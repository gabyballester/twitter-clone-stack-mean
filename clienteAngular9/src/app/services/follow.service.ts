// Definimos objetos
import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable } from "rxjs/Observable";
import { GLOBAL } from "./global";
import { Follow } from "../models/follow";

@Injectable()
export class FollowService {
  public url: string; //propiedad url

  // constructor pasándole http client para poder usarlo
  constructor(private _http: HttpClient) {
    this.url = GLOBAL.url; //le damos valor
  }

  // Método para guardar el follow en BBDD
  // pasamos estos datos por parámetro
  addFollow(token, follow): Observable<any> {
    //Recibe un observable
    let params = JSON.stringify(follow); //pasamos a string follow
    let headers = new HttpHeaders()
      .set("Content-Type", "application/json") //Recogemos headers
      .set("Authorization", token); //seteamos el token en authorization;

    //retornamos petición para añadir el follow
    return this._http.post(this.url + "follow", params, { headers: headers });
  }

  // Método para borrar el follow en BBDD
  deleteFollow(token, id): Observable<any> {
    //Recibe un observable
    let headers = new HttpHeaders()
      .set("Content-Type", "application/json") //Recogemos headers
      .set("Authorization", token); //seteamos el token en authorization;

    //retornamos petición para borrar el follow con ese id
    return this._http.delete(this.url + "follow/" + id, { headers: headers });
  }

  // Método para recibir los following
  getFollowing(token, userId = null, page = 1): Observable<any> {
    //Pasamos token, recibe observable
    let headers = new HttpHeaders()
      .set("Content-Type", "application/json") //Recogemos headers
      .set("Authorization", token); //seteamos el token en authorization;

    var url = this.url + "following";
    if (userId != null) {
      url = this.url + "following/" + userId + "/" + page;
    }

    return this._http.get(url, { headers: headers });
  }

  // Método para recibir los followed
  getFollowed(token, userId = null, page = 1): Observable<any> {
    //Pasamos token, recibe observable
    let headers = new HttpHeaders()
      .set("Content-Type", "application/json") //Recogemos headers
      .set("Authorization", token); //seteamos el token en authorization;

    var url = this.url + "followed";
    if (userId != null) {
      url = this.url + "followed/" + userId + "/" + page;
    }

    return this._http.get(url, { headers: headers });
  }

  // Recupero usuarios que me siguen
  getMyFollows(token): Observable<any> {
    console.log("Entra en FollowService.getMyfollows");

    //Pasamos token, recibe observable
    let headers = new HttpHeaders()
      .set("Content-Type", "application/json") //Recogemos headers
      .set("Authorization", token); //seteamos el token en authorization;
    console.log("Retorna follows");
    //ponemos true para que los devuelta todos
    return this._http.get(this.url + "get-my-follows/true", { headers });
  }
  
}

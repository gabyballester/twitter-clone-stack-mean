import { Injectable } from '@angular/core';
import { GLOBAL } from './global';

@Injectable()
export class UploadService {
    public url: string;

    constructor() {
        this.url = GLOBAL.url;
    }

    makeFileRequest(
        url: string, //url del método de la API que vamos a usar
        params: Array<string>, //params que será un array de tipo strings
        files: Array<File>, //files que será un array de tipo file
        token: string, //para reconocer al usuario, token tipo string
        name: string) { //name para darle nombre a la imagen tipo string
        //Devolvemos una promesa con función anónima y recibe resolve o reject
        return new Promise(function (resolve, reject) {
            //Creamos variable para el formulario clásico
            var formData: any = new FormData();
            /** Variable xhr con un objeto que permite hacer
             * peticiones AJAX en javascript */
            var xhr = new XMLHttpRequest
            //Recorremos al array de files
            for (var i = 0; i < files.length; i++) {
                /** Adjuntamos a la petición
                 * 1. El nombre del fichero
                 * 2. El registro que está recorriendo en ese momento
                 * 3. El nombre del registro*/

                formData.append(name, files[i], files[i].name);
            }

            //Ahora comprobamos estados de xhr
            xhr.onreadystatechange = function () {
                //comprobamos que el estado ready es 4 para continuar
                if (xhr.readyState == 4) {
                    //que el estado es 200
                    if (xhr.status == 200) {
                        resolve(JSON.parse(xhr.response));
                    } else {
                        reject(xhr.response);
                    }
                }
            }

            //Finalmente hacemos petición AJAX por post a la url con true
            xhr.open('POST', url, true);
            //seteamos cabecera de Authorization con el token
            xhr.setRequestHeader('Authorization', token);
            //Finalmente se envía formData con todos los ficheros
            xhr.send(formData);
        });
    }
}
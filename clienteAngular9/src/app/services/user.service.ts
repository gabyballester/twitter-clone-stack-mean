//Definimos objetos
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { GLOBAL } from './global';
import { User } from '../models/user';

//Con el decorador inyectable le vamos a decir que podemos usarlo en cualquier componente
@Injectable()
export class UserService {
    //Variable de la url del backend
    public url: string;
    public user: User;
    public identity: any;
    public token: string;
    public stats;


    //Decimos que vamos a usar el httpclient
    constructor(public _http: HttpClient) {
        //Damos valor a la variable en el constructor dentro del constructor
        this.url = GLOBAL.url;
    }

    //Paso el parámetro user del tipo user al método register
    //Le indicamos que nos va a devolver un observable
    register(user: User): Observable<any> {
        //Convierto a string el json user
        let params = JSON.stringify(user);
        //Seteamos cabeceras
        let headers = new HttpHeaders().set('Content-Type', 'application/json');

        //Petición a la api
        //Devuelvo petición http post con la ruta base + register de este componente
        //Paso params (objeto user) y headers recogidos previamente
        return this._http.post(this.url + 'register', params, { headers: headers });
    }

    signup(user: User, gettoken = null): Observable<any> {
        if (gettoken != null) {
            user.gettoken = gettoken;
        }
        let params = JSON.stringify(user);//Convierto a string el json user
        //Seteamos cabeceras
        let headers = new HttpHeaders().set('Content-Type', 'application/json');

        /*Petición a la api con la ruta base + /login
        Paso params (objeto user+gettoken si es 2ª petición) y headers recogidos previamente*/
        return this._http.post(this.url + 'login', params, { headers: headers });
    }

    getIdentityFromLocalStorage() { //parseamos a formato javascript para volver a tener el objeto
        let identity = JSON.parse(localStorage.getItem('identity'));
        if (identity != "undefined") {
            this.identity = identity;
        } else {
            this.identity = null;
        }
        return this.identity;
    }

    getTokenFromLocalStorage() {
        let token = JSON.parse(localStorage.getItem('token'));
        if (token != "undefined") {
            this.token = token;
        } else {
            this.token = null;
        }
        return this.token;
    }

    //Método para sacar los contadores, recibirá un observable tipo any
    getCounters(userId = null): Observable<any> {
        //  Definimos cabeceras
        let headers = new HttpHeaders().set('Content-Type', 'application/json')
            //Seteamos la autorización
            .set('Authorization', this.getTokenFromLocalStorage());

        //Si nos llega un userid 
        if (userId != null) {
            //Lo retornamos como petición get.url+ruta counters + userid, y las cabeceras
            return this._http.get(this.url + 'counters/' + userId, { headers: headers });

        } else { //en caso contrario devolvemos lo mismo sin id
            return this._http.get(this.url + 'counters/', { headers: headers });

        }
    }

    /** Método para actualizar el usuario
     * 1. Le pasamos el user, tipo user a actualizar como parámetro
     * 2. Recibe un observable
     */
    updateUser(user: User): Observable<any> {
        //Recogemos todos los datos del usuario en variable params en formato JSON STRING
        let params = JSON.stringify(user);
        //Le paso los headers adecuados (tipo de contenido y le indico que es un json)
        //Actualizamso la autorización que es el token
        let headers = new HttpHeaders().set('Content-Type', 'application/json')
            .set('Authorization', this.getTokenFromLocalStorage());
        //Retornamos la llamada AJAX put a la url update-user/:id
        return this._http.put(this.url + 'update-user/' + user._id, params, { headers: headers });
    }

    getStats() { //Recoger la información de counters del localstorage
        let stats = JSON.parse (localStorage.getItem('stats'));
        // parseamos json stats
        if (stats != "undefined") { //si es distinto de indefinido
            this.stats = stats; //stats será igual a los stats que llegan
        } else {
            this.stats = null //En caso contrario serán null
        }
        return this.stats; //Retornamos this.stats
    }

    //---Lo usaremos para sacar el listado de usuarios----///
    //Agregamos un nuevo método para sacar un listado de  los usuarios paginado.
    //parametro page = null y devuelve observable para usarlo en el componente
    getUsers(page = null): Observable<any> {
        let headers = new HttpHeaders().set('Content-Type', 'application/json')
            .set('Authorization', this.getTokenFromLocalStorage());
        return this._http.get(this.url + 'users/' + page, { headers: headers });
    }

    //---Lo usaremos para el perfil de usuario----///
    //Agregamos un nuevo método para sacar 1 único usuario
    //parametro page = null y devuelve observable para usarlo en el componente
    getUser(id): Observable<any> {
        let headers = new HttpHeaders().set('Content-Type', 'application/json')
            .set('Authorization', this.getTokenFromLocalStorage());
        return this._http.get(this.url + 'user/' + id, { headers: headers });
    }

}
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router'; //Importamos el router
import { Publication } from '../../models/publication'; //Importamos el modelo publication
import { UserService } from '../../services/user.service';
import { GLOBAL } from '../../services/global'; //Importamos el userservice
import { PublicationService } from '../../services/publication.service';
declare var jQuery: any;
declare var $: any;

@Component({
    selector: 'timeline',
    templateUrl: './timeline.component.html',
    styleUrls: ['./timeline.component.scss'],
    providers: [UserService, PublicationService]
})
export class TimelineComponent implements OnInit {
    public title: string;
    public identity;
    public token;
    public url: string;
    public status: string;
    public page;
    public total;
    public pages;
    public itemsPerPage; //agrego parámetro para recogerlo en el constructor
    public publications: Publication[]; //creo variable con array de publications
    public showImage;

    constructor(
        private _route: ActivatedRoute,
        private _router: Router,
        private _userService: UserService,
        private _publicationService: PublicationService
    ) {
        this.title = 'Timeline';
        this.identity = this._userService.getIdentityFromLocalStorage();
        this.token = this._userService.getTokenFromLocalStorage();
        this.url = GLOBAL.url;
        this.page = 1;
    }

    ngOnInit() {
        this.getPublications(this.page);
    }

    //método para conseguir todas las publicaciones de quienes seguimos
    getPublications(page, adding = false) { //recibe página como parámetro y adding a false

        this._publicationService.getPublications(this.token, page).subscribe(
            response => {

                if (response.publications) { //si llegan las publicaciones
                    this.total = response.total_items; //recogemos el numero de items
                    this.pages = response.pages;
                    this.itemsPerPage = response.itemsPerPage;

                    if (!adding) { //si adding es false
                        this.publications = response.publications; //lo asigno a la variable creada arriba
                    } else { //si llega true
                        //creamos array de publicaciones (pagina1)
                        var arrayA = this.publications;
                        //nuevo array que nos devuelve el api (pagina2)
                        var arrayB = response.publications;
                        //concatenamos ambos arrays
                        this.publications = arrayA.concat(arrayB);

                        $('html, body').animate({ scrollTop: $('body').prop("scrollHeight") }, 1000);
                    }

                    //Si el número de página es mayor o igual que el número de páginas
                    if (page >= this.pages) {
                        //que asigne true a noMore para que no se muestre el botón
                        this.noMore = true;
                    }
                } else {
                    this.status = 'error'; //si no llega, le indico status error
                }
            },
            error => {
                var errorMessage = <any>error;
                console.log(errorMessage);
                if (errorMessage != null) {
                    this.status = 'error';
                }
            }
        );
    }

    public noMore = false;
    viewMore() {
        //si la longitud el array es == al total -4 (cantidad de elementos por página)
        if (this.publications.length == (this.total)) {
            this.noMore = true;
        } else {
            this.page += 1;
        }
        // llamo al metodo pasándole la página y el adding a true
        this.getPublications(this.page, true);
    }

    refresh(event=null) {
        //la llamada a getPublications y le paso la página como parámetro
        this.getPublications(1); //ejecuto refresco a página 1
    }

    //método y propiedad no pueden tener el mismo nombre
    showThisImage(id) { //le pasamos el id de la publicación
        this.showImage = id; //asociamos el id que llegue
    }

    //método y propiedad no pueden tener el mismo nombre
    hideThisImage() { //le pasamos el id de la publicación
        this.showImage = 0; //asociamos el id que llegue
    }

    deletePublication(id){
       
        this._publicationService.deletePublication(this.token,id).subscribe(           
            response => {
                this.refresh()
            },
            error => {
                console.log(<any>error);   
            }
        );
    }

}

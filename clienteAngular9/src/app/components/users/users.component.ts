import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { User } from '../../models/user';
import { Follow } from '../../models/follow';
import { UserService } from '../../services/user.service';
import { FollowService } from '../../services/follow.service';
import { GLOBAL } from '../../services/global'; //Importamos el GLOBAL


@Component({
  selector: 'users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss'],
  providers: [UserService, FollowService]
})
export class UsersComponent implements OnInit {
  public title: string;
  public url: string;
  public identity;
  public token;
  public page;
  public next_page;
  public prev_page;
  public total;
  public pages;
  public users: User[]; //Array de tipo users
  public follows;
  public status;


  constructor(
    private _route: ActivatedRoute,
    private _router: Router,
    private _userService: UserService,
    private _followService: FollowService
  ) {
    this.title = 'Gente';
    this.url = GLOBAL.url;
    this.identity = this._userService.getIdentityFromLocalStorage();
    this.token = this._userService.getTokenFromLocalStorage();
  }

  ngOnInit() {
    this.actualPage();
  }

  actualPage() {
    //Recogemos el parámetro page
    this._route.params.subscribe(params => {
      let page = +params['page'] //recogemos el parámetro page como entero con el signo +
      this.page = page //damos valor a esa propiedad

      if (!params['page']) {
        page = 1;
      }

      if (!page) {    //Si la página no existe
        page = 1; //la pagina es = 1
      } else { //si existe
        this.next_page = page + 1; //la siguiente página será page + 1
        this.prev_page = page - 1; //la página previa será page - 1

        if (this.prev_page <= 0) {    //si la pgina previa es <= 0 es porque es la primera,
          this.prev_page = 1; //entonces la pagina previa será 1
        }

      }
      //Entonces llamaríamos al método para sacar el listado de usuarios
      this.getUsers(page);
    });

  }

  //Creamos nuevo método getusers
  getUsers(page) { //recibe la página
    //petición al servicio de angular al método getUsers y pasamos página
    this._userService.getUsers(page).subscribe(
      response => {
        if (!response) {
          this.status = 'error';
        } else {
          //Almaceno lo que devuelve el response en las variables creadas
          this.total = response.total;
          this.users = response.users;
          this.pages = response.pages;
          this.follows = response.users_following;

          if (page > response.pages) { //Si nos llega un número de página mayor al número de páginas (no existe)
            this._router.navigate(['/gente/', 1]); //Que redirija a /gente/1 página 1
          }

        }

      }, error => {
        var errorMessage = <any>error;
        console.log(errorMessage);

        if (errorMessage != null) {
          this.status = 'error';
        }
      }

    );
  }

  public followUserOver;
  mouseEnter(user_id) {
    this.followUserOver = user_id;
  }
  mouseLeave(user_id) {
    this.followUserOver = 0;
  }

  followUser(followed) {
    /** Nueva instancia de follow 
     * 1. Cargamos las propiedades a usar en este caso el id de identity
     * que será el usuario guardado en localStorage
     * 2. Por otro lado followed que es el usuario a seguir*/
    var follow = new Follow('', this.identity._id, followed);
    //llamamos al servicio y método pasando token y follow
    //Nos subscribimos a la respuesta
    this._followService.addFollow(this.token, follow).subscribe(

      //Si todo va bien
      response => {

        if (!response.follow) { //Si no llega response follow
          this.status = 'error';
        } else {
          this.status = 'success'; //Si llega
          this.follows.push(followed); //Que agregue ese id al array de follows
        }
      }, error => {
        var errorMessage = <any>error;
        console.log(errorMessage);

        if (errorMessage != null) {
          this.status = 'error';
        }
      }
    );
  }

  unfollowUser(followed) {
    this._followService.deleteFollow(this.token, followed).subscribe(
      response => {
        //busco followed en el array de follows y lo guardalo en i
        var search = this.follows.indexOf(followed);
        //Si lo encuentra será diferente a -1
        // (si no lo encuentra, el resultado de indexOf es -1)
        if (search != -1) {
          //Le decimos que borre 1 elemento a partir de ese
          this.follows.splice(search, 1);
        }

      }, error => {
        var errorMessage = <any>error;
        console.log(errorMessage);

        if (errorMessage != null) {
          this.status = 'error';
        }
      }
    );
  }

}

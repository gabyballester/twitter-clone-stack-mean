import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { User } from '../../models/user';
//Importamos UserService
import {UserService} from '../../services/user.service';

//Agregamos el decorador y le pasamos propiedades o metadatos
@Component({
    selector: 'register', //etiqueta donde se va a cargar
    templateUrl: './register.component.html',
    //Aquí incluimos todos los servicios disponibles en esta clase
    providers: [UserService]
})
export class RegisterComponent implements OnInit {
    //Propiedad title tipo string
    public title: string;
    //Propiedad user de tipo User
    public user: User;
    public status: string;

    constructor(
        //Asignamos valores a estas propiedades para usar el router
        private _route: ActivatedRoute,
        private _router: Router,
        private _userService: UserService
    ) {
        this.title = 'Regístrate';
        //Damos valor a la propiedad user:User
        //Las cogemos una a una del modelo y asignando string vacío a todos
        this.user = new User('', '', '', '', '', '', 'ROLE_USER', '', '');
        // this.user = new User("", "admin", "admin", "admin", "admin@admin.es", "asdfasdf", "ROLE_ADMIN", "");
        //_id, name, surname, nick, email, password, role, image
    }

    ngOnInit() {

    }
    onSubmit(form){ //pasamos como parámetro para vaciar el formulario
        //LLamada al userService y método registro, paso el user y recibo un observable
        this._userService.register(this.user).subscribe(
            response=>{
                if(response.user && response.user._id){
                    console.log(response.user);
                    //En caso de ser correcto, sacamos mensaje positivo
                    this.status = 'success';
                    form.reset();
                } else {
                    this.status = 'error';
                }
            },
            error=>{
                console.log(<any>error);
            }
        );
    }
}

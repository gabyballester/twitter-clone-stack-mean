import { Component, OnInit, Input } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router'; //Importamos el router
import { Publication } from '../../models/publication'; //Importamos el modelo publication
import { UserService } from '../../services/user.service';
import { GLOBAL } from '../../services/global'; //Importamos el userservice
import { PublicationService } from '../../services/publication.service';
declare var jQuery: any;
declare var $: any;

@Component({
    selector: 'publications',
    templateUrl: './publications.component.html',
    styleUrls: ['./publications.component.scss'],
    providers: [UserService, PublicationService]
})
export class PublicationsComponent implements OnInit {
    public title: string;
    public identity;
    public token;
    public url: string;
    public status: string;
    public page;
    public total;
    public pages;
    public itemsPerPage; //agrego parámetro para recogerlo en el constructor
    public publications: Publication[]; //creo variable con array de publications
    @Input() user: string;

    constructor(
        private _route: ActivatedRoute,
        private _router: Router,
        private _userService: UserService,
        private _publicationService: PublicationService
    ) {
        this.title = 'Publicaciones';
        this.identity = this._userService.getIdentityFromLocalStorage();
        this.token = this._userService.getTokenFromLocalStorage();
        this.url = GLOBAL.url;
        this.page = 1;
    }

    ngOnInit() {       
        this.getPublications(this.user, this.page);
    }

    //método para conseguir todas las publicaciones de quienes seguimos
    getPublications(user, page, adding = false) { //recibe página como parámetro y adding a false

        this._publicationService.getPublicationsUser(this.token, user, page).subscribe(
            response => {               
                if (response.publications) { //si llegan las publicaciones
                    this.total = response.total_items; //recogemos el numero de items
                    this.pages = response.pages;
                    this.itemsPerPage = response.itemsPerPage;

                    if (!adding) { //si adding es false
                        this.publications = response.publications; //lo asigno a la variable creada arriba
                    } else { //si llega true
                        //creamos array de publicaciones (pagina1)
                        var arrayA = this.publications;
                        //nuevo array que nos devuelve el api (pagina2)
                        var arrayB = response.publications;
                        //concatenamos ambos arrays
                        this.publications = arrayA.concat(arrayB);

                        $('html, body').animate({scrollTop: $('body').prop("scrollHeight")}, 1000);
                    }

                    //Si el número de página es mayor o igual que el número de páginas
                    if (page >= this.pages) {
                        //que asigne true a noMore para que no se muestre el botón
                        this.noMore = true;
                    }
                } else {
                    this.status = 'error'; //si no llega, le indico status error
                }
            },
            error => {
                var errorMessage = <any>error;
                console.log(errorMessage);
                if (errorMessage != null) {
                    this.status = 'error';
                }
            }
        );
    }

    public noMore = false;
    viewMore() {
        //si la longitud el array es == al total -4 (cantidad de elementos por página)
        if (this.publications.length == (this.total)) {
            this.noMore = true;
        } else {
            this.page += 1;
        }
        // llamo al metodo pasándole la página y el adding a true
        this.getPublications(this.user, this.page, true);
    }    
}

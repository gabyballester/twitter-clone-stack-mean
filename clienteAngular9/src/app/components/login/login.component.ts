//Importamos lo básico
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { User } from '../../models/user';
import { UserService } from '../../services/user.service';

//Agregamos el decorador y le pasamos propiedades o metadatos
@Component({
    selector: 'login', //etiqueta donde se va a cargar
    templateUrl: './login.component.html',
    providers: [UserService]
})
export class LoginComponent implements OnInit {
    //Propiedad title tipo string
    public title: string;
    //Propiedad user de tipo User
    public user: User;
    public status: String;
    public identity;
    public token: String;
    public gettoken: String;

    constructor(
        //Aquí damos valor a las propiedades declaradas en la clase
        private _route: ActivatedRoute,
        private _router: Router,
        private _userService: UserService
    ) {
        this.title = 'Identifícate';
        this.user = new User('', '', '', '', '', '', 'ROLE_USER', '', '');

    }

    ngOnInit() {

    }

    onSubmit() {
        this._userService.signup(this.user).subscribe(
            response => {
                this.identity = response.user;
                console.log(this.identity);
                if (!this.identity || !this.identity._id) {
                    this.status = 'error';
                } else {
                    //Persistimos datos usuario en localStorage
                    localStorage.setItem('identity', JSON.stringify(this.identity));

                    //Conseguir token
                    this.gettingToken();
                }
            },
            error => {
                var errorMessage = <any>error;
                if (errorMessage != null) {
                    this.status = 'error';
                }
            }
        )
    }

    gettingToken() {
        this._userService.signup(this.user, 'true').subscribe(
            response => {
                this.token = response.token;
                if (this.token) {
                    console.log(this.token);
                    if (this.token.length <= 0) {
                        this.status = 'error';
                    } else {
                        // Persistimos el token en localStorage
                        localStorage.setItem('token', JSON.stringify(this.token));

                        // Conseguir los contadores
                        this.getCounters();
                    }
                } else {
                    console.log('No llegó el token');
                }

            },
            error => {
                var errorMessage = <any>error;
                console.log(errorMessage);
                if (errorMessage != null) {
                    this.status = 'error';
                }
            }
        )
    }

    getCounters() {
        //llamamos al servicio y nos suscribimos a la respuesta
        this._userService.getCounters().subscribe(
            response => {
                //Si todo va bien consoleamos response
                localStorage.setItem('stats', JSON.stringify(response));
                this._router.navigate(['/']);
                this.status = 'success';
            },
            error => {
                console.log(<any>error);
            }
        )
    }

}
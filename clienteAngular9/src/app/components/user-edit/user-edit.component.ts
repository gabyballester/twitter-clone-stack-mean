import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router'; //Importamos el router
import { User } from '../../models/user'; //Importamos el modelo user
import { UserService } from '../../services/user.service'; //Importamos el userservice
import { UploadService } from '../../services/upload.service'; //Importamos el userservice
import { GLOBAL } from '../../services/global'; //Importamos el userservice

@Component({
  selector: 'user-edit', //Cambio el selector
  templateUrl: './user-edit.component.html',
  styleUrls: ['./user-edit.component.css'],
  providers: [UserService, UploadService] //Importo los services
})
export class UserEditComponent implements OnInit {
  //Agrego propiedades necesarias
  public title: string;
  public user: User;
  public identity;
  public token;
  public status: string;
  public url: string;

  constructor(
    //Inyectamos las cosas del router
    private _route: ActivatedRoute,
    private _router: Router,
    private _userService: UserService,
    private _uploadService: UploadService
  ) {
    this.title = 'Actualizar mis datos'; //Atribuimos este valor a title
    this.user = this._userService.getIdentityFromLocalStorage(); //Capturo identity
    this.identity = this.user; //Asignamos user a identity
    this.token = this._userService.getTokenFromLocalStorage(); //Capturamos el token
    this.url = GLOBAL.url;
  }

  ngOnInit() {

  }

  onSubmit() {
    //LLamamso sl servicio update pasamos el user y nos suscribimos a la respuesta del callback
    this._userService.updateUser(this.user).subscribe(
      response => {
        if (!response.user) { //Si el user no nos llega
          this.status = 'error'; //asiganmos error a status
        } else { //si todo es correcto
          this.status = 'success';  //asignamos success a status
          //seteamos identity en localStorage estringifeando el user
          localStorage.setItem('identity', JSON.stringify(this.user));
          //Asignamos a identity el user actualizado
          this.identity = this.user;
        }
        //Subida de imagen de usuario
        this._uploadService.makeFileRequest(
          this.url + 'upload-image-user/'+ this.user._id, [],
          this.filesToUpload, this.token, 'image')
          .then((result: any) => {
            this.user.image = result.user.image; //Asignamos la imagen al usuario
             //Actualizamos identity en localStorage con nuevo usuario
            localStorage.setItem('identity', JSON.stringify(this.user))
          });
      },
      error => {
        //Si recibe un error lo asignamos al status
        var errorMessage = <any>error;
        this.status = 'error'
      }
    );
  }

  //---------Método que captura los archivos del input del formulario
  //variable Array de objetos tipo fichero
  public filesToUpload: Array<File>;
  fileChangeEvent(fileInput: any) { //Parámetro el file input tipo any
    this.filesToUpload = <Array<File>>fileInput.target.files; //asignamos todos los ficheros a este objeto
  }

}

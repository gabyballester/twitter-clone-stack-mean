import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router'; //Importamos el router
import { User } from '../../models/user';
import { Follow } from '../../models/follow';
import { UserService } from '../../services/user.service';
import { FollowService } from '../../services/follow.service';
import { GLOBAL } from '../../services/global'; //Importamos el userservice

@Component({
    selector: 'profile',
    templateUrl: './profile.component.html',
    styleUrls: ['./profile.component.scss'],
    providers: [UserService, FollowService]
})

export class ProfileComponent implements OnInit {
    public title: string;
    public user: User;
    public status: string;
    public identity;
    public token;
    public url;
    public stats;
    public followed; //para ver si nos sigue
    public following; //para ver si lo seguimos


    constructor(
        private _route: ActivatedRoute,
        private _router: Router,
        private _userService: UserService,
        private _followService: FollowService
    ) {
        this.title = 'Perfil';
        this.identity = this._userService.getIdentityFromLocalStorage();
        this.token = this._userService.getTokenFromLocalStorage();
        this.url = GLOBAL.url;
        this.followed = false;
        this.following = false;
    }

    ngOnInit() {
        this.loadPage(); //llamamos a este método para que se ejecute en cadena
    }

    //este método llamará a getuser pero sacando los params de la url
    loadPage() {
        this._route.params.subscribe(params => { //recogemos los parámetros
            let id = params['id']; //recogemos el id
            //ejecutamos estos métodos para que se ejecute en cadena y les pasamos el id de usuario
            this.getUser(id); 
            this.getCounters(id);
        });
    }

    //método para conseguir datos de usuario
    getUser(id) { //le pasamos un id
        //se conecta a la api con userService pasándole el id 
        this._userService.getUser(id).subscribe( // nos subscribimos a la respuesta
            response => {
                if (response.user) {
                    this.user = response.user; //asignamos user a user
                    //comprobamos existe following y si existe ese id
                    if (response.following && response.following._id) {
                        this.following = true; //seteamos la propiedad a true
                    } else { this.following = false; } //de lo contrario lo seteamos a false
                    //comprobamos existe followed y si existe ese id
                    if (response.followed && response.followed._id) { //comprobamos si nos sigue
                        this.followed = true; //seteamos la propiedad a true
                    } else { this.followed = false; } //de lo contrario lo seteamos a false
                } else {
                    this.status = 'error';
                }
            }, error => {
                console.log(<any>error);
                //en caso de error, redirigimos a su propio perfil, porque el perfil solicitado no existe
                this._router.navigate(['/perfil', this.identity._id]);
            }
        );
    }

    //para traer los stats
    getCounters(id) { //le pasamos el id de ususario
        this._userService.getCounters(id).subscribe(
            response => {
                this.stats = response; //asignamos lo que nos trae response a stats
            },
            error => {
                console.log(<any>error);
            }
        );
    }

    //seguir a quién nos sigue
    followUser(followed) {//parámetro del id de quién no sigue
        //id vacío, user es el registrado y followed quién le pasamos el id
        var follow = new Follow('', this.identity._id, followed)

        //llamamos el servicio followService y método addFollow
        //le pasamos los parámetros necesarios (token y follow para enviarlo al servicio)
        this._followService.addFollow(this.token, follow).subscribe(
            response => {
                //si todo es ok following será true, o sea que lo estaremos siguiendo
                //esto cambiará el botón en el front
                this.following = true;
            },
            error => {
                console.log(<any>error);
            }
        );
    }

    //dejar de seguir a quién seguimos
    unfollowUser(followed) { //parámetro del id de quién seguimos
        //llamamos el servicio followService y método deleteFollow
        //le pasamos los parámetros necesarios (token y followed para enviarlo al servicio)
        this._followService.deleteFollow(this.token, followed).subscribe(
            response => {
                //si todo es ok following será true, o sea que lo estaremos siguiendo
                //esto cambiará el botón en el front
                this.following = false;
            },
            error => {
                console.log(<any>error);
            }
        );
    }

    //propiedad que asignaremos user_id o 0.
    public followUserOver;

    //método para cuando entramos en el botón
    mouseEnter(user_id){ //pasamos el user_id
        this.followUserOver = user_id; //lo asignamos
    }
    //método cuando el ratón sale de encima del botón
    mouseLeave(){
        this.followUserOver = 0; //asignamos 0
    }
}
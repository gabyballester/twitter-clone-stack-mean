import { Component, OnInit, EventEmitter, Input, Output } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { UserService } from '../../services/user.service';
import { GLOBAL } from '../../services/global'; //Importamos el GLOBAL
import { Publication } from '../../models/publication'; //Importo modelo publication
import { PublicationService } from '../../services/publication.service';
import { UploadService } from '../../services/upload.service'; //importo el servicio

@Component({
    selector: 'sidebar',
    templateUrl: './sidebar.component.html',
    styleUrls: ['./sidebar.component.scss'],
    //declaramos los servicios en providers
    providers: [UserService, PublicationService, UploadService]
})

export class SidebarComponent implements OnInit {
    public identity; //propiedad para almacenar usuario logueado
    public token;
    public stats;
    public url;
    public status;
    public publication: Publication; //declaro propiedad de tipo x

    constructor(
        private _route: ActivatedRoute,
        private _router: Router,
        private _uploadService: UploadService,
        private _userService: UserService,
        private _publicationService: PublicationService
    ) {
        this.identity = this._userService.getIdentityFromLocalStorage();
        this.token = this._userService.getTokenFromLocalStorage();
        this.stats = this._userService.getStats();
        this.url = GLOBAL.url;
        this.publication = new Publication(
            "", "", "", "", this.identity._id
        );
    }

    ngOnInit() {

    }

    //método para el formulario de publicación
    onSubmit(form, $event) { //pasamos form para resetear el formulario
        this._publicationService.addPublication(this.token, this.publication).subscribe(
            response => {
                if (response.publication) {
                    // this.publication = response.publication;

                    if (this.filesToUpload && this.filesToUpload.length) {
                        //Subir imagen – llamo al servicio y al método de subida
                        this._uploadService.makeFileRequest(
                            this.url  //Le paso la url de la api
                            + 'upload-image-pub/' //concateno la ruta completa
                            /**  Concateno el id de la publication que es el objeto 
                             * que devuelve el método principal addPublication */
                            + response.publication._id,
                            [], //parámetros vacíos
                            this.filesToUpload, //ficheros a subir
                            this.token, //token de usuario
                            'image' //nombre del campo de fichero que recoge el backend
                        )
                            .then((result: any) => { //promesa callback result tipo any
                                //asignamos a file la imagen que devuelve la api
                                this.publication.file = result.image;
                                this.status = 'success'; //seteamos resultado ok
                                form.reset(); //reseteo del formulario
                                this._router.navigate(['/timeline']); //redirijo a /timeline
                                this.sended.emit({ send: 'true' }); //a sended emit le paso send: true
                            });
                    } else {
                        this.status = 'success'; //seteamos resultado ok
                        form.reset(); //reseteo del formulario                        
                        this._router.navigate(['/timeline']); //redirijo a /timeline
                        this.sended.emit({ send: 'true' }); //a sended emit le paso send: true
                    }
                } else {
                    this.status = 'error';
                }
            },
            error => {
                var errorMessage = <any>error;
                console.log(errorMessage);
                if (errorMessage != null) {
                    this.status = 'error';
                }
            }
        );
    }

    //este array se va rellenando al ejecutar el método fileChangeEvent
    public filesToUpload: Array<File>;
    //Array que se va rellenando al ejecutarse este método
    fileChangeEvent(fileInput: any) {
        //seleccionamos los ficheros que le estoy pansando como parámetro
        this.filesToUpload = <Array<File>>fileInput.target.files;
        console.log(this.filesToUpload);

    }

    // output
    @Output() sended = new EventEmitter(); //propiedad tipo eventemitter

    sendPublication(event) {//a este método le pasamos el evento
        this.sended.emit({ send: 'true' }); //a sended emit le paso send: true
    }

}
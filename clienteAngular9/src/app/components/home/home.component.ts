import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'home', //etiqueta donde se va a cargar
    templateUrl: './home.component.html',
})

export class HomeComponent implements OnInit {
    public title: string;
    constructor() {
        this.title = "Bienvenido a NGSOCIAL"
    }
    ngOnInit() {

    }
}
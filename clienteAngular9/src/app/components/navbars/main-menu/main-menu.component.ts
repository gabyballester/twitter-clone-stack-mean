import { Component, OnInit, DoCheck } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { UserService } from '../../../services/user.service';
import { GLOBAL } from '../../../services/global'; //Importamos el GLOBAL
declare var jQuery: any;
declare var $: any;

@Component({
  selector: 'app-main-menu',
  templateUrl: './main-menu.component.html',
  styleUrls: ['./main-menu.component.scss'],
  providers: [UserService]
})
export class MainMenuComponent implements OnInit, DoCheck {
  public title: string;
  public identity;
  public token;
  public url: string;

  constructor(
    private _route: ActivatedRoute,
    private _router: Router,
    private _userService: UserService
  ) {
    this.title = "NGSOCIAL";
    this.url = GLOBAL.url;
  }

  ngOnInit() {
    //traemos identity del localStorage a través del user.service
    this.identity = this._userService.getIdentityFromLocalStorage();
    //Traemos el token
    this.token = this._userService.getTokenFromLocalStorage();
  }

  ngDoCheck() {
    this.identity = this._userService.getIdentityFromLocalStorage();
  }

  logout(){
    localStorage.clear();
    this.identity = null;
    this._router.navigate(['/']);
  }

}

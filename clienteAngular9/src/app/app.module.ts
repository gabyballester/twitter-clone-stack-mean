import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
//Importamos módulo de formularios
import { FormsModule } from '@angular/forms';
//Importamos el httpmodule
import { HttpClientModule } from '@angular/common/http';
//Importamos el routing creado
import { routing, appRoutingProviders } from './app.routing';
import { MomentModule } from 'angular2-moment';

// Módulo Messages
import { MessagesModule } from './messages/messages.module';

//Carga de componentes
import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { HomeComponent } from './components/home/home.component';
import { MainMenuComponent } from './components/navbars/main-menu/main-menu.component';
import { UserEditComponent } from './components/user-edit/user-edit.component';
import { UsersComponent } from './components/users/users.component';
import { SidebarComponent} from './components/sidebar/sidebar.component';
import { TimelineComponent} from './components/timeline/timeline.component';
import { PublicationsComponent} from './components/publications/publications.component';
import { ProfileComponent } from './components/profile/profile.component';
import { FollowingComponent } from './components/following/following.component';
import { FollowedComponent } from './components/followed/followed.component';

// Servicios
import {UserService} from './services/user.service';
import {UserGuard} from './services/user.guard';

@NgModule({
  //Aquí metemos las directivas, componentes y pipes
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    HomeComponent,
    MainMenuComponent,
    UserEditComponent,
    UsersComponent,
    SidebarComponent,
    TimelineComponent,
    PublicationsComponent,
    ProfileComponent,
    FollowingComponent,
    FollowedComponent
  ],
  imports: [
    BrowserModule, FormsModule, HttpClientModule, routing, MomentModule,
    MessagesModule
  ],
  providers: [appRoutingProviders, UserService, UserGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }

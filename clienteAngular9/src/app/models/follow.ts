export class Follow {
    constructor(
        //Creo todas las propiedades tal como el modelo de BBDD
        public _id: string,
        public user: String,
        public followed: String
    ){}
}
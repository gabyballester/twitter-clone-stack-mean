export class Publication {
    constructor(
        //Creo todas las propiedades tal como el modelo de BBDD
        public _id: string,
        public text: String,
        public file: String,
        public created_at: String,
        public user: String,
    ){}
}
export class Message {
    constructor(
        //Creo todas las propiedades tal como el modelo de BBDD
        public _id: string,
        public text: String,
        public viewed: String,
        public created_at: String,
        public emitter: String,
        public receiver: String
    ){}
}
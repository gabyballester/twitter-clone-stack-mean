export class User {
    constructor(
        //Creo todas las propiedades tal como el modelo de BBDD
        public _id: string,
        public name: String,
        public surname: String,
        public nick: String,
        public email: String,
        public password: String,
        public role: String,
        public image: String,
        public gettoken: String
    ){}
}
'use strict'

//requerimos la librería del orm
var mongoose = require('mongoose');

/** Cargamos var app con toda la configuración de express
 * (el fichero en sí)
 * No hace falta poner .js porque ya lo reconoce automáticamente */
var app = require('./app');

// Indicamos el puerto en el que vamos a trabajar
var port = 3800;


// Conexión Database
mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost:27017/curso_mean_social', {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false
})
    //Si la conexión se realiza correctamente que saque un console log
    .then(() => {
        console.log("La conexión a la base de datos se ha realizado correctamente")

        //Crear servidor
        app.listen(port, () => {
            console.log('Servidor corriendo en http://localhost:3800')
        })
    })
    //Que recoja el error si sucede con el catch
    .catch(err => console.log(err));
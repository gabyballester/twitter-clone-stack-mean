'use strict'

var mongoosePaginate = require('mongoose-pagination');

//Cargamos modelo user
var User = require('../models/user');
//Cargamos modelo follow
var Follow = require('../models/follow');

function saveFollow(req, res) {
    var params = req.body; //recogemos params del body
    var follow = new Follow(); //Usamos objeto new follow, creado en modelo
    //El usuario seguidor, guarda el objeto del usuario identificado
    follow.user = req.user.sub //En el authenticated  req.user = payload;
    //El usuario seguido, guarda el id del usuario que seguimos
    follow.followed = params.followed;

    follow.save((err, followStored) => {
        if (err) return res.status(500).send({ message: 'Error al guardar el seguimiento' });
        //Si no nos llega nada del stored
        if (!followStored) return res.status(404).send({ message: 'El seguimiento no se ha guardado' });

        return res.status(200).send({ follow: followStored });
    });
}

function deleteFollow(req, res) {
    //Recogemos el id del usuario actual que llega del sub
    var userId = req.user.sub;
    //Recogemos el id del usuario a dejar de seguir que llega de params
    var followId = req.params.id;
    /** Función de búsqueda en la BBDD
     * 1. Busco registro que tenga el userid y followed concretos
     * 2. Utilizo el método remove con un callback err*/
    Follow.find({ 'user': userId, 'followed': followId }).remove(err => {
        //Si llega ese error que retorne 500 y mensaje
        if (err) return res.status(500).send({ message: 'Error al dejar de seguir' });
        //Si todo va bien retornamos 200 y mensaje
        return res.status(200).send({ message: 'El follow se ha eliminado' });
    })
}

function getFollowingUsers(req, res) {
    /** Recogeremos el usuario logueado
    * Pero si nos llegan los parámetros id y page por la url
    * entonces el userId será el del id del param*/
    var userId = req.user.sub;
    if (req.params.id && req.params.page) {
        userId = req.params.id;
    }

    //Página por defecto 1)
    var page = 1
    //Comprobamos si nos llega la página por params y la cambiamos
    if (req.params.page) {
        page = req.params.page;
    } else {//Si no existe page que page sea el id
        page = req.params.id
    }
    //4 usuarios por página
    var itemsPerPage = 4;

    /** Consulta a la bbdd
     * 1. Buscamos todos los follows asociados a userid logueado
     * 2. Populamos la información del campo followed y cambiarla por el documento original
     * correspondiente a ese objectid, usando un json e indicando el path del campo que quiero
     * sustituir por el objeto al que hace referencia */

    Follow.find({ user: userId }).populate({ path: 'followed' }).paginate(page, itemsPerPage, (err, follows, total) => {
        //Si llega ese error que retorne 500 y mensaje
        if (err) return res.status(500).send({ message: 'Error en el servidor' });
        //Si no hay follows que devuelva 404 y mensaje
        if (!follows) return res.status(404).send({ message: 'No sigues a nadie' });
        //Si todo va bien, requerimos los ids de usuario seguidos con un callback.
        followUserIds(req.user.sub).then((value) => { //le paso el usuario logueado y devuelve value
            return res.status(200).send({
                total: total, //total de documentos que devuelve el find
                pages: Math.ceil(total / itemsPerPage),//Media del total por pagina
                page,
                follows, //objeto completo con todos los follows
                users_following: value.following, //Los que seguimos
                users_follow_me: value.followed, //Los seguidores
            });
        });
    });

}

//Función asíncrona, recibe user_id
async function followUserIds(user_id) {

    //Creo variable para guardar el array de usuarios que sigo con find recupera todos   
    var followingArray = await Follow.find({ "user": user_id });

    var followingArray_clean = [] //Creo array vacío
    followingArray.forEach((followingItem) => { //Recorro array con forEach
        followingArray_clean.push(followingItem.followed); //Voy metiendo en el array cada item de usuario del follow
    });

    //Creo variable para guardar el array de usuarios que me siguen con find recupera todos    
    var followedArray = await Follow.find({ "followed": user_id });

    var followedArray_clean = [] //Creo array vacío
    followedArray.forEach((followedItem) => { //Recorro array con forEach
        followedArray_clean.push(followedItem.user); //Voy metiendo en el array cada item de usuario del follow
    });

    return { //Retornamos objetos following y followed ambos tipo array de user ids limpios
        following: followingArray_clean,
        followed: followedArray_clean
    }
}

function getFollowedUsers(req, res) {

    /** Recogeremos el usuario logueado
     * Pero si nos llegan los parámetros id y page por la url
     * entonces el userId será el del id del param*/
    var userId = req.user.sub;
    if (req.params.id && req.params.page) {
        userId = req.params.id;
    }

    //Página por defecto 1)
    var page = 1
    //Comprobamos si nos llega la página por params y la cambiamos
    if (req.params.page) {
        page = req.params.page;
    } else {//Si no existe page que page sea el id
        page = req.params.id
    }
    //4 usuarios por página
    var itemsPerPage = 4;

    /** Consulta a la bbdd
     * 1. Buscamos todos los followed que coincidan con nuestro userid logueado
     * 2. Populamos la información del campo followed y user y cambiarla por el documento original
     * correspondiente a ese objectid, indicando directamente los documentos de BBDD que quiero
     * sustituir por el objeto al que hace referencia */
    Follow.find({ followed: userId }).populate('user').paginate(page, itemsPerPage, (err, follows, total) => {
        //Si llega ese error que retorne 500 y mensaje
        if (err) return res.status(500).send({ message: 'Error en el servidor' });
        //Si no hay follows que devuelva 404 y mensaje
        if (!follows) return res.status(404).send({ message: 'No te sigue nadie' });
        //Si todo va bien retornamos 200 y dentro

        followUserIds(req.user.sub).then((value) => { //le paso el usuario logueado y devuelve value
            return res.status(200).send({
                total: total, //total de documentos que devuelve el find
                pages: Math.ceil(total / itemsPerPage),//Media del total por pagina
                page,
                follows, //objeto completo con todos los follows
                users_following: value.following, //Los que seguimos
                users_follow_me: value.followed, //Los seguidores
            });
        });

    });

}

//Devolver listado usuarios
function getMyFollows(req, res) {
    //Recogemos usuario logueado
    var userId = req.user.sub;
    //Recogemos en find, la busqueda de follow con user id logueado
    var find = Follow.find({ user: userId });
    //si recibo followed par params entonces find será de followed con el user id
    if (req.params.followed) {
        var find = Follow.find({ followed: userId });
    }
    //Buscamos el contenido de find y populamos user y followed
    find.populate('user followed').exec((err, follows) => {
        //Si llega ese error que retorne 500 y mensaje
        if (err) return res.status(500).send({ message: 'Error en el servidor' });
        //Si no hay follows que devuelva 404 y mensaje
        if (!follows) return res.status(404).send({ message: 'No sigues a nadie' });
        //Si todo va bien retornamos 200 y dentro

        return res.status(200).send({ follows }) //objeto completo con todos los follows
    });

}

module.exports = {
    saveFollow,
    deleteFollow,
    getFollowingUsers,
    getFollowedUsers,
    getMyFollows
}
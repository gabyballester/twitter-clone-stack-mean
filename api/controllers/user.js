'use strict'
var bcrypt = require('bcrypt-nodejs');
var mongoosePaginate = require('mongoose-pagination');
var fs = require('fs');
var path = require('path');

var User = require('../models/user'); //Cargamos el modelo de user para ser utilizado
var Follow = require('../models/follow'); //Cargamos el modelo de follow para ser utilizado
var Publication = require('../models/publication');
var jwt = require('../services/jwt'); //Cargamos el jwt

/** Métodos de prueba */

//Creamos function home
function home(req, res) {
    res.status(200).send({
        message: 'Hola mundo esto es el Home'
    });
}

//Creamos function pruebas
function pruebas(req, res) {
    console.log(req.body);
    res.status(200).send({
        message: 'Acción de pruebas en el servidor de Node JS'
    });
}

/** Método para el registro */
function saveUser(req, res) {
    //Recogemos todos los datos que lleguen por post
    var params = req.body;
    var user = new User();

    //si recibe todos los datos
    if (params.name && params.surname && params.nick &&
        params.email && params.password) {
        user.name = params.name;
        user.surname = params.surname;
        user.nick = params.nick;
        user.email = params.email;
        user.role = 'ROLE_USER';
        user.image = null;

        //Controlar usuarios duplicados 
        User.find({
            $or: [
                { email: user.email.toLowerCase() },
                { nick: user.nick.toLowerCase() }
            ]
        }).exec((err, users) => {
            if (err) return res.status(500).send({ message: 'Error en la peticion de usuarios' });
            if (users && users.length >= 1) {
                return res.status(200).send({ message: 'El usuario ya existe' });
            } else {

                //Ciframos el password y guardamos los datos
                bcrypt.hash(params.password, null, null, (err, hash) => {
                    //asignamos el password encriptado a user.password
                    user.password = hash;
                    //método de guardado de mongoose
                    user.save((err, userStored) => {
                        if (err) return res.status(500).send({ message: 'Error al guardar el usuario' });
                        if (userStored) {
                            res.status(200).send({ user: userStored });
                        } else {
                            res.status(400).send({ message: 'No se ha registrado el usuario' });
                        }
                    });
                });
            }
        });
    } else {
        res.status(200).send({
            message: 'Envía todos los campos necesarios!!'
        })
    }
}

/** Método para login */
function loginUser(req, res) {
    var params = req.body; //recogemos parametros del body

    var email = params.email; //variable con el email
    var password = params.password; //variable con el pass

    //Esto sería busca uno where email es este
    User.findOne({ email: email }, (err, user) => {
        //si hay error termina el proceso
        if (err) return res.status(500).send({ message: 'Error en la petición' });
        //si hay user entonces que compruebe password
        if (user) {
            /** si el password encriptado es igual al password encriptado del user.password
             * Devuelve un callback */
            bcrypt.compare(password, user.password, (err, check) => {
                if (check) {

                    if (params.gettoken) { //comprobamos si llega el tokeno
                        //Generar y devolver el token
                        return res.status(200).send({
                            //El token será el createtoken de user
                            token: jwt.createToken(user)
                        })
                    } else {
                        //devolver datos usuario si los passwords coinciden
                        return res.status(200).send({ user })
                    }

                } else {
                    //si no coinciden, cerramos la ejecución
                    return res.status(404).send({ message: 'Password incorrecto!!' });
                }
            });
        } else {
            //si el user no se encuentra en la base de datos
            return res.status(404).send({ message: 'Usuario no encontrado' });
        }

    });
}

/** Conseguir datos de un usuario */
function getUser(req, res) {
    var userId = req.params.id; //Recogemos el userid por la url

    User.findById(userId, (err, user) => {
        //Si hay error
        if (err) return res.status(500).send({ message: 'Error en la petición' });
        //Si no llega usuario
        if (!user) return res.status(404).send({ message: 'El usuario no existe' });
        /** LLamada a la función asíncrona followThisUser
         * 1. Pasamos el usuario sub (logueado) y el userId(como parametro por url)
         * 2. Ahora como llamamos a una promesa, usamos .then pasando value como callback
         * 3. Si nos devuelve value, retornamos status 200 y devuelvo user y value (following y followed)*/
        followThisUser(req.user.sub, userId)
            .then((value) => {  //Si va OK
                return res.status(200).send({
                    user,
                    following: value.following,
                    followed: value.followed
                });
            })
            .catch(console.error) //Si falla
    });
}

//Función asíncrona, recibe identity (logueado) y userid(consultado)
async function followThisUser(identity_user_id, user_id) {
    //Con esto recogemos si yo sigo al usuario y si ponemos find encuentra duplicados
    const following = await Follow.findOne({ "user": identity_user_id, "followed": user_id })

    //Con esto recogemos si el usuario me sigue a mi y si ponemos find encuentra duplicados
    const followed = await Follow.findOne({ "user": user_id, "followed": identity_user_id })

    //Devolvemos el valor de ambos
    return {
        following: following,
        followed: followed
    }
}

/** Devolver un listado de usurios paginado */
//esta propiedad viene del servicio, sub: user._id;
//Creamos la variable page
//Comprobamos que nos llega el número de página
//la recogemos en una variable page (sin var?)
function getUsers(req, res) {
    var identity_user_id = req.user.sub;
    var page = 1;
    if (req.params.page) {
        page = req.params.page;
    }

    //Cantidad de elementos por página 5 usuarios por ahora
    var itemsPerPage = 5;


    /** 
     * 3. Si hay error devolvemos 500 y mensaje
     * 4. Si no llega ningún usuario retornamos 404 no hay usuarios
     * 5. Si está todo correcto devolvemos un objeto con todos los datos
     * //usuarios
     * //nº total de registros
     * //nº paginas totales que habrá, divite total por nº items
     */
    /**1. Buscamos en base de datos todo y lo ordenamos por id 
     * 2. Usamos el método paginate, le pasamosla página, el items per page y un callback con:
     *      a. err (por si hay error)
     *      b. users( todos los usuarios)
     *      c. total (nº total de registros que hay en la base de datos)*/
    User.find().sort('_id').paginate(page, itemsPerPage, (err, users, total) => {
        //Si hay error devolvemos 500 y mensaje
        if (err) return res.status(500).send({ messsage: 'Error en la petición' });
        //Si no llega ningún usuario retornamos 404 no hay usuarios
        if (!users) return res.status(404).send({ message: 'No hay usuarios disponibles' });
        //Hacemos la llamada a la función asyíncrona
        followUserIds(identity_user_id)
            .then((value) => { //Si va todo ok
                //Si está todo correcto devolvemos un objeto con todos los datos que necesitamos

                return res.status(200).send({
                    users, //objeto usuarios (en el modelo hemos quitado el password)
                    users_following: value.following, //Los que seguimos
                    users_follow_me: value.followed, //Los seguidores
                    total, //Nº total de páginas
                    pages: Math.ceil(total / itemsPerPage), //Nº de items por página
                    page
                });
            })
            .catch(console.error);  //Si falla
    });
}


//Función asíncrona, recibe user_id
async function followUserIds(user_id) {

    //Creo variable para guardar el array de usuarios que sigo con find recupera todos   
    var followingArray = await Follow.find({ "user": user_id });

    var followingArray_clean = [] //Creo array vacío
    followingArray.forEach((followingItem) => { //Recorro array con forEach
        followingArray_clean.push(followingItem.followed); //Voy metiendo en el array cada item de usuario del follow
    });

    //Creo variable para guardar el array de usuarios que me siguen con find recupera todos    
    var followedArray = await Follow.find({ "followed": user_id });

    var followedArray_clean = [] //Creo array vacío
    followedArray.forEach((followedItem) => { //Recorro array con forEach
        followedArray_clean.push(followedItem.user); //Voy metiendo en el array cada item de usuario del follow
    });

    return { //Retornamos objetos following y followed ambos tipo array de user ids limpios
        following: followingArray_clean,
        followed: followedArray_clean
    }
}

//Función que devuelve los contadores de cuanta gente seguimos, nos sigue y publicaciones
function getCounters(req, res) {
    var userId = req.user.sub;
    if (req.params.id) {
        var userId = req.params.id;
    }

    getCountFollow(userId).then((value) => {
        return res.status(200).send(value);
    });
}

//Función asíncrona para los contadores de los follows
async function getCountFollow(user_id) {

    //Contamos los que seguimos como usuario
    var following = await Follow.countDocuments({ "user": user_id }, ((err, count) => {
        if (err) return handleError(err);
        return count;
    }));


    //Contamos los que nos siguen como following_me
    var followed = await Follow.countDocuments({ "followed": user_id }, ((err, count) => {
        if (err) return handleError(err);
        return count;
    }));

    //Contamos las publicaciones de usuario
    var publications = await Publication.countDocuments({ "user": user_id }, ((err, count) => {
        if (err) return handleError(err);
        return count;
    }));

    return {
        following: following,
        following_me: followed,
        publications: publications
    }
}

//Edición de datos de usuario
function updateUser(req, res) {
    var userId = req.params.id; //Recogemos la id a actualizar
    var update = req.body; //Recogemos los datos que le pasaremos para actualizar

    // //Borramos propiedad password
    // delete update.password;

    /** Comprobamos que el usuario que se quiere modificar es el mismo que está autenticado con el token
     * Si no lo es que devuelva error 500 */
    if (userId != req.user.sub) {
        return res.status(500).send({ message: 'No tienes permiso para actualizar los datos del usuario' });
    }
    /** En el caso de que coincidan y sea Ok
     * 1. Busco y actualizo el user por id que le paso por parámetro
     * 2. un objeto json como new true para que devuelva el objeto actualizado desde la BBDD
     * 3. Y que actualice con el objeto update la bbdd
     * 4. Creo callback y gestiono error y recojo userUPdated */

    //Buscamos si existe al menos 1 registro con el email o el nick que pretendemos actualizar
    //dentro del objeto update recogido por el body
    User.find({
        $or: [
            { email: update.email.toLowerCase() },
            { nick: update.nick.toLowerCase() }
        ]
    }).exec((err, users) => { //Lanzamos la ejecución y nos devuelve err y users que coincidan
        //Creamos variable para guardarlo si existe un usuario con ese email o nick
        var user_isset = false;
        users.forEach((user) => { //Recorremos array de users en busca del user
            if (user && user.id != userId) user_isset = true; //Si existe cambiamos la variable a true
        });
        //Si la variable está seteada a true, retorna mensaje de error y para la ejecución
        if (user_isset) return res.status(404).send({ message: 'Dirección de correo o nick ya en uso' });

        User.findByIdAndUpdate(userId, update, { new: true }, (err, userUpdated) => {
            //Si hay un error
            if (err) return res.status(500).send({ messsage: 'Error en la petición' });
            //Si no recibimos el objeto userUpdated
            if (!userUpdated) return res.status(404).send({ message: 'No se ha actualizado el usuario' });
            //Si todo ok, devolvemos status 200 y el objeto modificado de la BBDD
            return res.status(200).send({ user: userUpdated });
        });
    });
}

//Subir archivos de imagen/avatar de usuario
function uploadImage(req, res) {
    var userId = req.params.id; //Recogemos la id a actualizar

    //Si estamos enviando algún fichero (existe files)
    if (req.files) {
        //Sacamos la ruta completa del archivo a subir que estamos enviando por post
        var file_path = req.files.image.path;
        console.log(file_path);
        //Cortamos el nombre del archivo y lo mete en una array separando elementos por \.
        var file_split = file_path.split('\\');
        console.log(file_split);

        //Creo variable para el nombre
        var file_name = file_split[2];
        console.log(file_name);

        //Separamos el nombre un un array
        var ext_split = file_name.split('\.');
        console.log(ext_split);

        //Sacamos el elemento 1 que es la extensión
        var file_ext = ext_split[1];
        console.log(file_ext);

        /** Comprobación del usuario a modificar ---
         * Que sea el mismo que está autenticado con el token */
        if (userId != req.user.sub) {
            console.log('USUARIO INCORRECTO');
            /** Si el usuario no coincide
             * 1. llamamos al método de borrado de archivo
             * 2. Le pasamos la response, file path y mensaje a mostrar */
            return removeFilesOfUploads(res, file_path, 'No tiene permisos para actualizar');
        } else {
            console.log('USUARIO CORRECTO');
        }

        //Esto comprueba que la extensión es válida
        if (file_ext == 'png' || file_ext == 'jpg' || file_ext == 'jpeg' || file_ext == 'gif') {
            console.log('ARCHIVO CORRECTO');
            /** Actualizar documento de usuario logueado
             * 1. Al objeto user metodo busarporidyactualizar
             * 2. Parámetros: userid, objeto json con la propiedad a cambiar que es la imagen
             * cuyo valor será el file_name (nombre_archivo)
             * 3. Que devuelva el objeto renovado de la BBDD
             * 4. Callback para gestionar err y objeto userUpdated
             * 4.1 Dentro la gestión de error previa del usuario
             */

            User.findByIdAndUpdate(userId, { image: file_name }, { new: true }, (err, userUpdated) => {
                //Si hay un error
                if (err) return res.status(500).send({ messsage: 'Error en la petición' });
                //Si no recibimos el objeto userUpdated
                if (!userUpdated) return res.status(404).send({ message: 'No se ha actualizado el usuario' });
                //Si todo ok, devolvemos status 200 y el objeto modificado de la BBDD
                return res.status(200).send({ user: userUpdated });
            });

        } else {
            console.log('ARCHIVO ERRONEO');
            /** Si la extensión es incorrecta
             * 1. LLamamos al método de borrado de archivo
             * 2. Le pasamos la response, file path y mensaje a mostrar */
            return removeFilesOfUploads(res, file_path, 'Extensión no válida');
        }
    } else {
        return res.status(200).send({ message: 'No se ha subido la imagen' })
    }
}

/** Creamos funcionalidad para borrar ficheros
 * cuando el usuario o extensión no son válidos. */
//Le pasamos una ruta completa con nombre de archivo para borrar y el mensaje
function removeFilesOfUploads(res, file_path, message) {
    //Esto borra el archivo no válido en sí
    fs.unlink(file_path, (err) => {
        //Si pasa por aquí es que la extensión no es válida
        return res.status(200).send({ message: message });
    });
}

//Función para devolver imagen de usuario

function getImageFile(req, res) {
    //Variable imagefile donde recogemos por los parámetros la imagen en sí
    var image_file = req.params.imageFile;
    //Variable con la ruta completa a la carpeta uploads/users + imagen
    var path_file = './uploads/users/' + image_file;

    //Comprobamos que esa ruta entera existe, pasamos la ruta completa, y el callback
    fs.exists(path_file, (exists) => {
        //Si existe que devuelta el fichero en crudo
        if (exists) {
            res.sendFile(path.resolve(path_file));
        } else {
            //si no, 200, y mensaje 
            res.status(200).send({ message: 'No existe la imagen' });
        }
    });
}

//Exportamos métodos para ser usados en otros ficheros
module.exports = {
    home,
    pruebas,
    saveUser,
    loginUser,
    getUser,
    getUsers,
    getCounters,
    updateUser,
    uploadImage,
    getImageFile
}
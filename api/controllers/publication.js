'use strict'

var path = require('path');
var fs = require('fs');
var moment = require('moment');
var mongoosePaginate = require('mongoose-pagination');

var Publication = require('../models/publication'); //Cargamos modelo de Publication
//var User = require('../models/user'); //Cargamos el modelo de User
var Follow = require('../models/follow'); //Cargamos el modelo de Follow

//Creamos function pruebas
function probando(req, res) {
    res.status(200).send({
        message: 'Hola desde el Publication Controller'
    });
}

/** Método para guardar publicaciones */
function savePublication(req, res) {
    var params = req.body; //Recogemos los params por el body
    //Si no llega el param texto, retorna 200 y mensaje 
    if (!params.text) return res.status(200).send({ message: 'Debes enviar un texto' });
    //Creamos variable publication donde metemos una nueva instancia de publication
    var publication = new Publication();
    //Seteamos las propiedades
    publication.text = params.text;
    publication.file = 'null';
    publication.user = req.user.sub;
    publication.created_at = moment().unix();
    //Guardamos la publicación
    publication.save((err, publicationStored) => {
        if (err) return res.status(500).send({ message: 'Error al guardar la publicación' });
        if (!publicationStored) return res.status(404).send({ message: 'PUblicación no guardada' });
        return res.status(200).send({ publication: publicationStored });
    });
}


//Función que devuelva los post de usuarios que seguimos
async function getPublications(req, res) {

    var user = req.user.sub;

    var page = 1; //Variable page 1
    if (req.params.page) { //Si nos llega page por parámetro
        page = req.params.page //Entonces la page será esa y no 1
    }

    var itemsPerPage = 4; //Elementos por pagína = 4

    var following_dirty = await Follow.find({ user }).populate('followed');
    var following_clean = []; //Creo array vacío de seguimientos
    following_dirty.forEach((followingItem) => { //Recorro array con forEach
        //Voy metiendo en el array cada item de usuario del follow 
        following_clean.push(followingItem.followed);
    });
    following_clean.push(req.user.sub)//pusheamos nuestro id para mostrar nuestras publicaciones

    //Busco las publicaciones de cada uno de los usuarios que hay en el array following_clean
    Publication.find({ user: { "$in": following_clean } })
        .sort('-created_at') //ordenamos de manera ascendente
        .populate('user') //poblamos la propiedad user con todo el objeto user
        //indicamos paginas a mostrar e items por página, callback de err, publications y total
        .paginate(page, itemsPerPage, (err, publications, total) => {
            if (err) return res.status(500).send({ message: 'Error al devolver publicaciones' });
            if (!publications) return res.status(404).send({ message: 'No hay publicaciones' });

            return res.status(200).send({
                total_items: total,
                pages: Math.ceil(total / itemsPerPage),
                page: page,
                itemsPerPage: itemsPerPage,
                publications
            });
        });
}

//Función que devuelva los post del usuario logueado
function getPublicationsUser(req, res) {

    var page = 1; //Variable page 1
    if (req.params.page) { //Si nos llega page por parámetro
        page = req.params.page //Entonces la page será esa y no 1
    }

    //parámetro user_id para sacar las publicaciones sólo de ese id
    var user = req.user.sub;

    //Si recibimos un parámetro user_id por la url, entonces ese será el usado
    if (req.params.user) {
        user = req.params.user;
    }

    var itemsPerPage = 4; //Elementos por pagína = 4

    Publication.find({ user: user }) //Busco las publicaciones del usuario logueado
        .sort('-created_at') //ordenamos de manera ascendente
        .populate('user') //poblamos la propiedad user con todo el objeto user
        //indicamos paginas a mostrar e items por página, callback de err, publications y total
        .paginate(page, itemsPerPage, (err, publications, total) => {
            if (err) return res.status(500).send({ message: 'Error al devolver publicaciones' });
            if (!publications) return res.status(404).send({ message: 'No hay publicaciones' });

            return res.status(200).send({
                total_items: total,
                pages: Math.ceil(total / itemsPerPage),
                page: page,
                itemsPerPage: itemsPerPage,
                publications
            });
        });
}


//Método para sacar 1 única publicación dado un id
function getPublication(req, res) {
    var publicationId = req.params.id; //recogemos el id de params

    //Buscamos la publicación con ese id
    Publication.findById(publicationId, (err, publication) => {
        if (err) return res.status(500).send({ message: 'Error al devolver publicaciones' });
        if (!publication) return res.status(404).send({ message: 'No existe la publicación' });
        return res.status(200).send({ publication }); //Retorno el objeto publication
    });
}

//Método para borrar 1 publicación
async function deletePublication(req, res) {
    var userId = req.user.sub;
    var publicationId = req.params.id;
    try {
        var publicationToDelete = await Publication.findOne({ 'user': userId, '_id': publicationId });
        if (publicationToDelete) {
            if (publicationToDelete.user == userId) {
                Publication.findByIdAndDelete(publicationId, ((err, publicationRemoved) => {
                    if (err) res.status(500).send({ message: 'Borrado fallido' });
                    return res.status(200).send({ publication: publicationRemoved });
                }));
            }
        }
    }
    catch (ex) {
        return res.status(401).send({ message: 'No tienes permisos para borrar esta publicación' });
    }
}

//Subir archivos de imagen de publicacion
function uploadImage(req, res) {
    var publicationId = req.params.id; //Recogemos la id de la publicación a actualizar
    var userLogged = req.user.sub;

    //Si estamos enviando algún fichero (existe files)
    if (req.files) {
        //Sacamos la ruta completa del archivo a subir que estamos enviando por post
        var file_path = req.files.image.path;
        //Cortamos el nombre del archivo y lo mete en una array separando elementos por \.
        var file_split = file_path.split('\\');
        //Creo variable para el nombre
        var file_name = file_split[2];
        //Separamos el nombre un un array
        var ext_split = file_name.split('\.');
        //Sacamos el elemento 1 que es la extensión
        var file_ext = ext_split[1];
        //Esto comprueba que la extensión es válida
        if (file_ext == 'png' || file_ext == 'jpg' || file_ext == 'jpeg' || file_ext == 'gif') {

            Publication.findOne({ 'user': userLogged, '_id': publicationId }).exec((err, publication) => {
                if (publication) {
                    /**1. Actualizar documento de usuario de la publicación con findbyidandupdate
                    * 2. Parámetros: userid, objeto json con la propiedad a cambiar que es file, 
                    *    cuyo valor será el file_name (nombre_archivo)
                    * 3. Que devuelva el objeto renovado de la BBDD
                    * 4. Callback para gestionar err y objeto userUpdated
                    * 4.1 Dentro la gestión de error previa del usuario */
                    Publication.findByIdAndUpdate(publicationId, { file: file_name }, { new: true }, (err, publicationUpdated) => {
                        //Si hay un error
                        if (err) return res.status(500).send({ messsage: 'Error en la petición' });
                        //Si no recibimos el objeto userUpdated
                        if (!publicationUpdated) return res.status(404).send({ message: 'No se ha actualizado la imagen' });
                        //Si todo ok, devolvemos status 200 y el objeto modificado de la BBDD
                        return res.status(200).send({ publication: publicationUpdated });
                    });
                } else {
                    /** Si la extensión es incorrecta
                     * 1. LLamamos al método de borrado de archivo
                     * 2. Le pasamos la response, file path y mensaje a mostrar */
                    return removeFilesOfUploads(res, file_path, 'No tiene permiso para actualizar la publicación');
                }
            });
        } else {
            return removeFilesOfUploads(res, file_path, 'Extensión no válida');
        }
    } else {
        return res.status(200).send({ message: 'No se ha subido la imagen' });
    }
}

/** Creamos funcionalidad para borrar ficheros
 * cuando el usuario o extensión no son válidos. */
//Le pasamos una ruta completa con nombre de archivo para borrar y el mensaje
function removeFilesOfUploads(res, file_path, message) {
    //Esto borra el archivo no válido en sí
    fs.unlink(file_path, (err) => {
        //Si pasa por aquí es que la extensión no es válida
        return res.status(200).send({ message: message });
    });
}

//Función para devolver imagen de publication
function getImageFile(req, res) {
    //Variable imagefile donde recogemos por los parámetros la imagen en sí
    var image_file = req.params.imageFile;
    //Variable con la ruta completa a la carpeta uploads/publications + imagen
    var path_file = './uploads/publications/' + image_file;

    //Comprobamos que esa ruta entera existe, pasamos la ruta completa, y el callback
    fs.exists(path_file, (exists) => {
        //Si existe que devuelta el fichero en crudo
        if (exists) {
            res.sendFile(path.resolve(path_file));
        } else {
            //si no, 200, y mensaje 
            res.status(200).send({ message: 'No existe la imagen' });
        }
    });
}

module.exports = {
    probando,
    savePublication,
    getPublications,
    getPublicationsUser,
    getPublication,
    deletePublication,
    uploadImage,
    getImageFile
}
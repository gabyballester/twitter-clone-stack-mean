'use strict'

var jwt = require('jwt-simple');
var moment = require('moment');
//Palabra secreta para generar el token
var secret = 'clave_secreta_curso_desarrollar_red_social_angular';

/** Exportamos el método con una función que recibe como parámetro
 * un user al que queremos generar el token */
exports.createToken = function(user){
    /** Propiedad payload con los datos del usuario para codificar
     * dentro del token */
    var payload = {
        sub: user._id, //propiedad id del usuario
        name: user.name,
        surname: user.surname,
        nick: user.nick,
        email: user.email,
        role: user.role,
        image: user.image,
        iat: moment().unix(), //fecha de creación del token
        exp: moment().add(30, 'days').unix() //fecha de expiración
    };

    /** Devolvemos codificado el payload (datos usuario)
     * Y la palabra secreta */
    return jwt.encode(payload, secret)
};
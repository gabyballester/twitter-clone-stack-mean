'use strict'

/** 1. Requerimos módulo de express para trabajar con rutas etc*/
var express = require('express');
/** 2. Requerimos body parser que va a transformar los datos que recibamos en una petición a un un objeto json */
var bodyParser = require('body-parser');

/** 3. Creamos una instancia de express dentro de la variable app */
var app = express();

//------Carga rutas
var user_routes = require('./routes/user');
var follow_routes = require('./routes/follow');
var publication_routes = require('./routes/publication');
var message_routes = require('./routes/message');

//------Middlewares 
/** Los middlewares se van a ejecutar en cada llamada */
/** 4. Este middleware transforma el body de la llamada a json */
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

//CORS Headers / Cabeceras CORS
// configurar cabeceras http
app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Authorization, X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Allow-Request-Method');
    res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE');
    res.header('Allow', 'GET, POST, OPTIONS, PUT, DELETE');
 
    next();
});


//Rutas
app.use('/api', user_routes);
app.use('/api', follow_routes);
app.use('/api', publication_routes);
app.use('/api', message_routes);

//Exportación
module.exports = app;
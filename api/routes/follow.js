'use strict'

//Importamos express y FollowController
var express = require('express');
//Cargamos el controlador de follows
var FollowController = require('../controllers/follow');

//Esto nos dará acceso a los métodos http -> get, post, put, delete...
var api = express.Router();
//Middleware que usaremos en todas las rutas para que compruebe si está autenticado
var md_auth = require('../middlewares/authenticated');

//Rutas
//Agregamos en esta ruta el middleware para securizarlo
api.post('/follow', md_auth.ensureAuth, FollowController.saveFollow);
api.delete('/follow/:id', md_auth.ensureAuth, FollowController.deleteFollow);
api.get('/following/:id?/:page?', md_auth.ensureAuth, FollowController.getFollowingUsers);
api.get('/followed/:id?/:page?', md_auth.ensureAuth, FollowController.getFollowedUsers);
api.get('/get-my-follows/:followed?', md_auth.ensureAuth, FollowController.getMyFollows);

module.exports = api;
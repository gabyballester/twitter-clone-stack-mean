'use strict'

//Importamos express y UserController
var express = require('express');
var PublicationController = require('../controllers/publication');

//Esto nos dará acceso a los métodos http -> get, post, put, delete...
var api = express.Router();
//Middleware que usaremos en todas las rutas para que compruebe si está autenticado
var md_auth = require('../middlewares/authenticated');

//Cargamos la librería de connect-multiparty
var multipart = require('connect-multiparty');
//Creamos variable middleware upload donde le decimos que subirá archivos a la carpeta indicada
var md_upload = multipart({uploadDir: './uploads/publications'});

//Rutas
api.get('/probando-pub', md_auth.ensureAuth, PublicationController.probando);
api.post('/publication', md_auth.ensureAuth, PublicationController.savePublication);
api.get('/publications/:page?', md_auth.ensureAuth, PublicationController.getPublications);
api.get('/publications-user/:user/:page?', md_auth.ensureAuth, PublicationController.getPublicationsUser);
api.get('/publication/:id', md_auth.ensureAuth, PublicationController.getPublication);
api.delete('/publication/:id', md_auth.ensureAuth, PublicationController.deletePublication);
api.post('/upload-image-pub/:id', [md_auth.ensureAuth, md_upload], PublicationController.uploadImage);
api.get('/get-image-pub/:imageFile', PublicationController.getImageFile);

module.exports = api;
'use strict'

//Importamos express y UserController
var express = require('express');
var UserController = require('../controllers/user');

//Esto nos dará acceso a los métodos http -> get, post, put, delete...
var api = express.Router();
//Middleware que usaremos en todas las rutas para que compruebe si está autenticado
var md_auth = require('../middlewares/authenticated');

//Cargamos la librería de connect-multiparty
var multipart = require('connect-multiparty');
//Creamos variable middleware upload donde le decimos que subirá archivos a la carpeta indicada
var md_upload = multipart({uploadDir: './uploads/users'});

//Rutas
api.get('/home', UserController.home);
//Agregamos en esta ruta el middleware para securizarlo
api.get('/pruebas', md_auth.ensureAuth, UserController.pruebas);
api.post('/register', UserController.saveUser);
api.post('/login', UserController.loginUser);
api.get('/user/:id', md_auth.ensureAuth, UserController.getUser);
api.get('/users/:page?', md_auth.ensureAuth, UserController.getUsers);
api.get('/counters/:id?', md_auth.ensureAuth, UserController.getCounters);
api.put('/update-user/:id', md_auth.ensureAuth, UserController.updateUser);
//Cuando tenemos que pasar variso middlewarese, se los pasamos como un array
api.post('/upload-image-user/:id', [md_auth.ensureAuth, md_upload], UserController.uploadImage);
api.get('/get-image-user/:imageFile', UserController.getImageFile)

module.exports = api;
'use strict'

//Importamos express y FollowController
var express = require('express');
//Cargamos el controlador de follows
var MessageController = require('../controllers/message');

//Esto nos dará acceso a los métodos http -> get, post, put, delete...
var api = express.Router();
//Middleware que usaremos en todas las rutas para que compruebe si está autenticado
var md_auth = require('../middlewares/authenticated');

//Rutas
//Agregamos en esta ruta el middleware para securizarlo
api.get('/probando-md', md_auth.ensureAuth, MessageController.probando);
api.post('/message', md_auth.ensureAuth, MessageController.saveMessage);
api.get('/my-messages/:page?', md_auth.ensureAuth, MessageController.getReceivedMessages);
api.get('/messages/:page?', md_auth.ensureAuth, MessageController.getEmittedMessages);
api.get('/unviewed-messages', md_auth.ensureAuth, MessageController.getUnviewedMessages);
api.get('/set-viewed-messages', md_auth.ensureAuth, MessageController.setViewedMessages);

module.exports = api;
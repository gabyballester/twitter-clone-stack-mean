'use strict'

var jwt = require('jwt-simple');
var moment = require('moment');
var secret = 'clave_secreta_curso_desarrollar_red_social_angular';
//Extraer la palabra secreta a un servicio
exports.ensureAuth = function (req, res, next) {
    /** El next permite salir del middleware y seguir cargando lo siguient que venta
     * en tiempo de ejecución */
    //El token nos llegará en una cabecera por lo tanto tenemos que comprobarlo
    if (!req.headers.authorization) {
        return res.status(401).send({
            message: 'La petición no tiene la cabecera de autenticación'
        });
    }
    /**
     * Guardamos el token lo que nos llega de la cabecera y reemplazamos comillas
     * simples y dobles, por nada para que lo quite
     */
    var token = req.headers.authorization.replace(/['"]+/g, '');

    /** Decodificamos el payload */
    try {
        var payload = jwt.decode(token, secret)
        //Si la expiración del payload es menor que la fecha actual
        if (payload.exp <= moment().unix()) {
            //Retornamos que la fecha ha expirado
            return res.status(401).send({
                message: 'El token ha expirado'
            });
        }
    } catch (ex) {
        //Si falla la decodificación, retornamos mensaje
        return res.status(401).send({
            message: 'El token no es válido'
        });
    }
    /** Metemos una propiedad en caliente al user para que esté disponible
      * en todos los controladores */
    req.user = payload;
    next(); //Para salir finalmente del middleware y efectuar la función del controlador
}
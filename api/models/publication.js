'use strict'

var mongoose = require('mongoose'); //requerimos la librería mongoose
var Schema = mongoose.Schema; //creamos variable esquema para poder usar el squema de mongoose

//Definimos las propiedades del modelo
var PublicationSchema = Schema({
    text: String,
    file: String,
    created_at: String,
    /** El campo user, se va a popular con un objeto id de otro documento 
     * que hará referencia a la entidad User */
    user: { type: Schema.ObjectId, ref: 'User' }
    /** Cuando hagamos el populate, sustituirá ese ObjectId
     * por los datos del User de esta publicación */
})

//Esportamos para poder utilizarlo en otros ficheros
//Cuando guardemos un objeto en BBDD lo pluralizará porque será una colección
module.exports = mongoose.model('Publication', PublicationSchema);

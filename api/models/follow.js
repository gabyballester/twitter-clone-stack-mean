'use strict'

var mongoose = require('mongoose'); //requerimos la librería mongoose
var Schema = mongoose.Schema; //creamos variable esquema para poder usar el squema de mongoose

//Definimos las propiedades del modelo
var FollowSchema = Schema({
    //propiedad user con un objeto referente a user
    user: { type: Schema.ObjectId, ref: 'User' },
    //propiedad followed con un objeto referente a user
    followed: { type: Schema.ObjectId, ref: 'User' },
});

//Exportamos para poder utilizarlo en otros ficheros
//Cuando guardemos un objeto en BBDD lo pluralizará porque será una colección
module.exports = mongoose.model('Follow', FollowSchema);
'use strict'

var mongoose = require('mongoose'); //requerimos la librería mongoose
var Schema = mongoose.Schema; //creamos variable esquema para poder usar el squema de mongoose

//creamos variable esquema para poder usar el squema de mongoose importado
var Schema = mongoose.Schema;

//Definimos las propiedades del modelo
var MessageSchema = Schema({
    text: String,
    viewed: String,
    created_at: String,
    emitter: { type: Schema.ObjectId, ref: 'User' },
    receiver: { type: Schema.ObjectId, ref: 'User' },
});

//Esportamos para poder utilizarlo en otros ficheros
//Cuando guardemos un objeto en BBDD lo pluralizará porque será una colección
module.exports = mongoose.model('Message', MessageSchema);
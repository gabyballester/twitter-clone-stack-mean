'use strict'

var mongoose = require('mongoose'); //requerimos la librería mongoose
var Schema = mongoose.Schema; //creamos variable esquema para poder usar el squema de mongoose

//Definimos las propiedades del modelo
var UserSchema = Schema({
    name: String,
    surname: String,
    nick: String,
    email: String,
    password: String,
    role: String,
    image: String
})

UserSchema.methods.toJSON = function () {
    const user = this;

    return {
       _id: user.id,
       name: user.name,
       surname: user.surname,
       nick: user.nick,
       email: user.email,
       role: user.role,
       image: user.image
    }
}

//Esportamos para poder utilizarlo en otros ficheros
//Cuando guardemos un objeto en BBDD lo pluralizará porque será una colección
module.exports = mongoose.model('User', UserSchema);
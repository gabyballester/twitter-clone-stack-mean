# Proyecto imitación a Twitter
Proyecto incremental que se irá implementando en función de la necesidad del momento.


### Srcipts de reconstrucción y arranque

- Carpeta front: clienteAngular9
- Carpeta back:  api
##### Reconstrucción
    npm install
##### Arranque
      npm start

#### Base de datos MONGO
Entidades del proyecto


 ![Entidades de la base de datos](screenshots/screenshot1.jpg) 

## Funcionalidades
- Registro
- Login
- Securizado de área de usuario y autenticación con jwt
- Permite seguir usuarios y publicar posts
- Publicaciones paginadas cargando cierto número de publicaciones en cada scroll.
- Permite modificar el perfil de usuario
    - Cambiar cualquiera de sus datos
    - Agregar avatar de usuario
    - Visualizar datos de usuario tipo panel

## Mejoras
- Añadir nuevas funcionalidades
- Revisar las actuales ya que algunas no han sido revisadas
- Aplicar testing de diferentes tipos
- Mejoras visuales, Grid, Flexbox en mayor media que CSS puro para ganar escalabilidad.